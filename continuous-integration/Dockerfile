FROM centos

ENV SOLR_VERSION      6.4.0
ENV PAYARA_URL        https://s3-eu-west-1.amazonaws.com/payara.fish/Payara+Downloads/Payara+4.1.1.164/payara-micro-4.1.1.164.jar
ENV BOUNCYCASTLE_VERSION  1.55
ENV BOUNCYCASTLE_URL  http://central.maven.org/maven2/org/bouncycastle/bcprov-jdk15on/${BOUNCYCASTLE_VERSION}/bcprov-jdk15on-${BOUNCYCASTLE_VERSION}.jar
ENV MAVEN_ARCHTYPE_VERSION 1.2.0

# Install mongodb, mariadb, jdk8, and maven
COPY mongodb-org-3.2.repo /etc/yum.repos.d/
COPY MariaDB.repo /etc/yum.repos.d/
RUN yum update -y && \
  yum install -y mongodb-org && \
  yum install -y MariaDB-server && \
  yum install -y java-1.8.0-openjdk-devel && \
  yum install -y maven && \
  yum install -y git && \
  yum clean all



# Install payara micro
RUN cd /opt && \
  mkdir payara && \
  cd payara && \
  curl -o payara-micro.jar ${PAYARA_URL} && \
  mkdir deployments && \
  touch /var/log/payara-micro.log



# Install Apache Solr
RUN cd /tmp && \
  curl -o solr-${SOLR_VERSION}.tgz http://apache.claz.org/lucene/solr/6.4.0/solr-6.4.0.tgz && \
  tar xzf solr-${SOLR_VERSION}.tgz solr-${SOLR_VERSION}/bin/install_solr_service.sh --strip-components=2 && \
  bash ./install_solr_service.sh solr-${SOLR_VERSION}.tgz -n -d /var/lib/solr && \
# Set up a sym link so solr logs in /var/log instead of /var/lib
  ln -sf /var/lib/solr/logs /var/log/solr && \
# Make the solr data folders
  mkdir /var/lib/solr/data/configsets && \
  mkdir /var/lib/solr/data/cores && \
  mkdir /var/lib/solr/data/indexes && \
# Remove some files that need to be replaced and the installer
  rm /var/lib/solr/log4j.properties && \
  rm /etc/default/solr.in.sh && \
  rm /tmp/*.tgz && \
# Make sure the solr user owns everything
  chown solr:solr /var/lib/solr -R

# Copy some solr configuration
COPY log4j.properties /var/lib/solr/
COPY solr.in.sh /etc/default/



# Copy the start scripts over and make sure they're executable.
COPY startmysql /usr/bin/
COPY startmongo /usr/bin/
COPY startsolr /usr/bin/
RUN chmod a+x /usr/bin/startmongo && \
  chmod a+x /usr/bin/startmysql && \
  chmod a+x /usr/bin/startsolr



# Copy BouncyCastle into the java security providers. This allows us to use whatever encryption we want. 
COPY java.security /tmp/
RUN cd $( dirname $( readlink -f /etc/alternatives/java)) && \
  cd ../lib && \
  rm security/java.security && \
  mv /tmp/java.security security/ && \
  curl -o ext/bcprov-jdk15on-${BOUNCYCASTLE_VERSION}.jar ${BOUNCYCASTLE_URL}


# Execute mvn clean package and verify on an empty project just so we include all of our base dependencies.
RUN cd /var/tmp && \
  mvn archetype:generate -DarchetypeGroupId=edu.byu.hbll -DarchetypeArtifactId=java-ee-project -DarchetypeVersion=${MAVEN_ARCHTYPE_VERSION} -DgroupId=edu.byu.hbll -DartifactId=project-test -Dversion=1.0.0 -Dpackage=edu.byu.hbll.projecttest -DarchetypeRepository=https://maven.lib.byu.edu/repository -DinteractiveMode=false && \
  cd project-test && \
  mvn clean package && \
  mvn verify; \
  cd .. && \
# Clean up the stuff we don't need
  rm -rf project-test



# Expose port 8080 for payara services
EXPOSE 8080, 8983
  
# ENTRYPOINT ["/bin/bash"]

